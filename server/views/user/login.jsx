import React from 'react';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: ''
    };
  }

  handleSubmit() {

  }

  render() {
    let stylesLogin,
      loginClasses = {
        stateRoot: 'zl-state--login login-state',
        masterKeeper: 'login-state__master-keeper login-master',
        hero: 'login-master__hero',
        form: 'login-master__form login-form',
        field: 'login-form__filed',
        btn: 'login-form__submit'
      };

    stylesLogin = '.login-state{background-color:#216dcc;height:100%;display:-webkit-box;' +
      'display:-ms-flexbox;display:flex;-ms-flex-flow:column nowrap;flex-flow:column;-webkit-box-align:center;' +
      '-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;' +
      'justify-content:center}' +
      '.login-state__master-keeper{background-color:#fff}' +
      '.login-master{width:350px;padding:20px;text-align:center}' +
      '.login-master__hero{font-size:25px;color:#ccc}.login-master__form{margin-top:30px}' +
      '.login-form{width:100%;height:100px;display:-webkit-box;display:-ms-flexbox;display:flex;' +
      '-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:justify;' +
      '-ms-flex-pack:justify;justify-content:space-between;-ms-flex-flow:column nowrap;flex-flow:column}' +
      '.login-form__filed{line-height:20px;width:100%}' +
      '.login-form__filed input{width:100%;box-sizing:border-box}' +
      '.login-form__submit{width:100%;line-height:30px;background-color:#03b5cc;color:#fff;text-align:center}' +
      'body,html{margin:0;padding:0}#root,body,html{height:100%;width:100%}';

    return (
      <div className={loginClasses.stateRoot}>
        <style> {stylesLogin}</style>
        <div className={loginClasses.masterKeeper}>
          <div className={loginClasses.hero}>inzly server</div>
          <div className={loginClasses.form}>
            <div className={loginClasses.field}>
              <input type="text" placeholder="Имя" value={this.state.userName}/>
            </div>
            <div className={loginClasses.field}>
              <input type="text" placeholder="Пароль" value={this.state.password}/>
            </div>
            <div className={loginClasses.btn} onClick={this.handleSubmit}>Войти</div>
          </div>
        </div>
      </div>
    );
  }
}


