function Inzly() {
  var io,
    gameSocket;

  /**
   * This function is called by index.js to initialize a new game instance.
   *
   * @param sio - The Socket.IO library
   * @param socket - The socket object for the connected client.
   */
  this.initGame = function (sio, socket) {
    io = sio;
    gameSocket = socket;
    gameSocket.emit('connected', {message: 'Welcome to the Inzly host!'});

    // Host events
    gameSocket.on('hostCreateNewGame', hostCreateNewGame);
  };

  /*
   * The 'START' button was clicked and hostCreateNewGame event occured.
   * */
  function hostCreateNewGame() {
    // Create a unique id of Socket.IO room
    var thisGameId = guid();

    // Return the Room ID (gameId) and the socket ID (mySocketId) to the browser client
    this.emit('newGameCreated', {gameId: thisGameId, mySocketId: this.id});

    // Join the game and wait for the players
    this.join(thisGameId.toString());
  }

  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }
}

module.exports = Inzly;
