import routesConfig from './routes/routes-config';

let Router;

Router = {
  init: function (app) {
    let routeController;

    for (let routeItem in routesConfig) {
      routeController = require(routesConfig[routeItem].controller);
      routeController.init(routesConfig[routeItem], app);

    }
  },
};

export default Router;
