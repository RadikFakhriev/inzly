import path from 'path';
import appRoot from 'app-root-path';

const PATHS = require(appRoot + '/constants/paths');

const routesConfig = {
  login: {
    url: '/',
    controller: path.join(PATHS.APP_SERVER_CONTROLLERS, 'user/login'),
    template: 'user/login'
  },
  game: {
    url: '/game',
    controller: path.join(PATHS.APP_SERVER_CONTROLLERS, 'game/game'),
    template: path.join(PATHS.APP_DIST, '/index.html')
  }
};

module.exports = routesConfig;
