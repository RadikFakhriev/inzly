import config from 'config';
import express from 'express';
import http from 'http';
import appRoot from 'app-root-path';
import socketIO from 'socket.io';
import passport from 'passport';
import expJsx from 'express-react-views';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import middlewaresLauncher from './MiddlewaresLauncher';
import Router from './Router';

const isDeveloping = process.env.NODE_ENV !== 'production',
  PATHS = require(appRoot + '/constants/paths'),
  APP_PORT = config.get('app.port');

class ApplicationServer {
  static App;
  static Server;
  static IO;
  static DevOptions;
  static ServerSystemMiddlewares;

  constructor() {
    console.log('Instance created!');
    ApplicationServer.App = express();
    ApplicationServer.Server = http.createServer(ApplicationServer.App).listen(APP_PORT);
    ApplicationServer.IO = socketIO.listen(ApplicationServer.Server);
    ApplicationServer.DevOptions = this.configureExpress();
    // ApplicationServer.ServerSystemMiddlewares = this.configureSystem();
  }

  configureExpress() {
    let expressApp = ApplicationServer.App,
      devOptions = null;

    expressApp.use(passport.initialize());
    expressApp.use(passport.session());

    expressApp.engine('jsx', expJsx.createEngine({
      beatify: true
    }));

    if (isDeveloping) {
      devOptions = this.prepareDevEnv();
    } else {
      expressApp.use(express.static(PATHS.APP_DIST));
    }

    expressApp.set('views', PATHS.APP_SERVER_VIEWS);
    expressApp.set('view engine', 'jsx');

    return devOptions;
  }

  configureSystem() {
    let ServerSystemMiddlewares;

    middlewaresLauncher.initAll();
    ServerSystemMiddlewares = middlewaresLauncher.getMiddlewareLayer();
    Router.init(ApplicationServer.App);

    return ServerSystemMiddlewares;
  }

  prepareDevEnv() {
    let compiler,
      devWebpackMiddleware,
      webpackDevConfig,
      expressApp = ApplicationServer.App;

    webpackDevConfig = require(appRoot + '/webpack.dev.config');

    compiler = webpack(webpackDevConfig);
    devWebpackMiddleware = webpackDevMiddleware(compiler, {
      publicPath: webpackDevConfig.output.publicPath,
      stats: {
        colors: true,
        hash: false,
        timings: true,
        chunks: false,
        chunkModules: false,
        modules: false
      }
    });

    expressApp.use(devWebpackMiddleware);
    expressApp.use(webpackHotMiddleware(compiler));

    return {isDeveloping, devWebpackMiddleware};
  }
}

const appInstance = new ApplicationServer();
Object.freeze(appInstance);

module.exports = appInstance;
