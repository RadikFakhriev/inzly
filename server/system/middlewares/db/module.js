import SequelizeAdapter from './dbConnector';

module.exports = {
    facade: SequelizeAdapter
};
