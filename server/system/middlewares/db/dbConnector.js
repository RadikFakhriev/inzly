import Sequelize from 'sequelize';
import config from 'config';

let sequelizeAdapter;

sequelizeAdapter = {
    sequelize: null,
    init: function () {
        this.sequelize = new Sequelize(
            config.app.database.username,
            config.app.database.password,
            config.app.database.options
        );
    }
};

export default sequelizeAdapter;



