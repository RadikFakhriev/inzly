import PassportAdapter from './PassportAdapter';

module.exports = {
    facade: PassportAdapter
};
