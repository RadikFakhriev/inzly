const passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    authMiddleware = require('./PassportMiddleware');

let PassportAdapter = {
    init: function () {
        let self = this;
        passport.serializeUser((user, cb) => {
            cb(null, user.username);
        });

        passport.deserializeUser((username, cb) => {
            self._findUser(username, cb);
        });

        self._initLocalStrategy();

        passport.authenticationMiddleware = authMiddleware;
    },

    _findUser: (username, callback) => {
        if (username === user.username) {
            return callback(null, user);
        }
        return callback(null);
    },

    _initLocalStrategy: function () {
        let self = this;
        passport.use(new LocalStrategy(
            (username, password, done) => {
                self._findUser(username, function (err, user) {
                    if (err) {
                        return done(err);
                    }
                    if (!user) {
                        return done(null, false);
                    }
                    if (password !== user.password) {
                        return done(null, false);
                    }
                    return done(null, user);
                });
            }
        ));
    }
};
export default PassportAdapter;
