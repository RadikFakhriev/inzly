import fs from 'fs';
import path from 'path';
import appRoot from 'app-root-path';

const PATHS = require(appRoot + '/constants/paths');

let MiddlewaresLauncher,
  middlewareDirNames,
  middlewareModulesPaths,
  SystemMiddlewares = {};

MiddlewaresLauncher = {
  _collectModulePaths: function () {
    middlewareDirNames = getDirectories(PATHS.APP_SERVER_MIDDLEWARE);

    middlewareModulesPaths = middlewareDirNames.map(function (item, idx) {
      return PATHS.APP_SERVER_MIDDLEWARE + item + '/module';
    });

    function getDirectories(srcpath) {
      return fs.readdirSync(srcpath).filter(function (file) {
        return fs.statSync(path.join(srcpath, file)).isDirectory();
      });
    }

    return middlewareModulesPaths;
  },

  initAll: function () {
    this._collectModulePaths();
    middlewareDirNames.forEach(function (middlewareName, idx) {
      SystemMiddlewares[middlewareName] = require(middlewareModulesPaths[idx]);
      SystemMiddlewares[middlewareName].facade.init();
    });
  },

  getMiddlewareLayer: function () {
    return SystemMiddlewares;
  }
};

export default MiddlewaresLauncher;




