const passport = require('passport');

let UserController = {
  init: function (pathsConfig, app) {
    let self = this;

    self.templatePath = pathsConfig.template;

    app.get(pathsConfig.url, self.executeRendering);
    app.post('/login', passport.authenticate('local', {
      successRedirect: '/game',
      failureRedirect: pathsConfig.url
    }));
  },

  executeRendering: (req, res) => {
    res.render(this.templatePath);
  },

};

module.exports = UserController;
