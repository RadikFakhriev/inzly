import path from 'path';
import appRoot from 'app-root-path';

const PATHS = require(appRoot + '/constants/paths');

let Inzly = require(PATHS.APP_SERVER_ROOT + '/Inzly'),
  ApplicationServer = require(PATHS.APP_SERVER_ROOT + '/system/ApplicationServer'),
  io;

let GameController = {
  init: function (pathsConfig, app) {
    let self = this;

    self.templatePath = pathsConfig.template;
    io = ApplicationServer.IO;

    app.get('/game', self.executeRendering);
    io.sockets.on('connection', function (socket) {
      var inzlyHostConnection = new Inzly();
      inzlyHostConnection.initGame(io, socket);
    });
  },

  executeRendering: (req, res) => {
    if (ApplicationServer.DevOptions.isDeveloping) {
      res.write(ApplicationServer.DevOptions.devWebpackMiddleware.fileSystem.readFileSync(this.templatePath));
      res.end();
    } else {
      res.sendFile(this.templatePath);
    }
  }
};

module.exports = GameController;
