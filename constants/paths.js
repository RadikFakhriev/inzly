import path from 'path';
import appRoot from 'app-root-path';

const PATHS = {
  APP_DIST: appRoot + '/dist/',
  CLIENT_APP: appRoot + '/client/app/',
  APP_SERVER_ROOT: appRoot + '/server/',
  APP_SERVER_MIDDLEWARE: appRoot + '/server/system/middlewares/',
  APP_SERVER_MODELS: appRoot + '/server/models/',
  APP_SERVER_VIEWS: appRoot + '/server/views/',
  APP_SERVER_CONTROLLERS: appRoot + '/server/controllers/',
  APP_SERVER_SYSTEM: appRoot + '/server/system/',
  APP_SERVER_ROUTES: appRoot + '/server/routes/'
};

module.exports = PATHS;
