'use strict';

var ExtractTextPlugin = require('extract-text-webpack-plugin'),
  path = require('path'),
  webpack = require('webpack'),
  HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  entry: [
    'webpack-hot-middleware/client?reload=true',
    path.join(__dirname, 'client/app/app-io.js'),
  ],
  output: {
    path: path.join(__dirname, '/dist'),
    filename: '[name].js',
    publicPath: '/'
  },
  resolve: {
    alias: {
      'socket.io-client': path.join(__dirname, 'node_modules', 'socket.io-client', 'dist', 'socket.io.js')
    }
  },
  module: {
    noParse: [/socket.io-client/],
    loaders: [{
      test: /\.styl$/,
      loader: ExtractTextPlugin.extract('stylus', 'css-loader!autoprefixer-loader!stylus-loader' +
        '?modules&localIdentName=[name]---[local]---[hash:base64:5]')
    }, {
      test: /\.js?$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        'presets': ['react', 'es2015', 'stage-0', 'react-hmre']
      }
    }, {
      test: /\.json?$/,
      loader: 'json'
    }],
  },
  plugins: [
    new ExtractTextPlugin('dev-bundle.css', {
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: 'client/app/index.tpl.html',
      inject: 'body',
      filename: 'index.html'
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  watch: true
};
