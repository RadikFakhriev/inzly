import io from 'socket.io-client';
import React from 'react';
import ReactDOM from 'react-dom';
import Login from '../login/Login';
import './app.styl';

let InzlyClientApp = {

  /*
   * Keep track of the gameId, which is identical to the ID of
   * the Socket.IO room used for the players communicate
   * */
  gameId: 0,

  /*
   * The Socket.IO socket object identifier. This is unique for
   * each player and host. It is generated when the browser initially
   * connects to the server when the page load for the first time.
   * */
  mySocketId: '',

  init: function () {
    ReactDOM.render(<Login />, document.getElementById('root'));
  }

};

InzlyClientApp.init();

export default InzlyClientApp;
