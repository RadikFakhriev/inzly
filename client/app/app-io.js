import io from 'socket.io-client';
import InzlyClientApp from './app';

let appIO = {

  /*
   * Entry point of client app.
   * This is called when the page is displayed.
   * It connect the Socket.IO client to the Socket.IO server.
   * */
  init: function () {
    appIO.socket = io.connect();
    appIO.bindEvents();
  },

  /*
   * Still connection is active, Socket.IO will listen to the following events emitted by
   * the Socket.IO server
   * */
  bindEvents: function () {
    appIO.socket.on('connected', appIO.onConnected);
    appIO.socket.on('newGameCreated', appIO.onNewGameCreated);
    appIO.socket.on('playerJoinedToRoom', appIO.playerJoinedToRoom);
    appIO.socket.on('beginNewGame', appIO.beginNewGame);
    appIO.socket.on('gameOver', appIO.gameOver);
    appIO.socket.on('error', appIO.error);
  },

  /*
   * The client is successfully connected!
   * */
  onConnected: function (data) {
    InzlyClientApp.mySocketId = appIO.socket.id;
    console.log(data);
  },

  onNewGameCreated: function () {
  },

  playerJoinedToRoom: function () {

  },

  beginNewGame: function () {

  },

  gameOver: function () {

  },

  error: function () {

  }
};

appIO.init();

export default appIO;
