import React from 'react';
import UserModel from './UserModel';
import './login.styl';

export default class Login extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            userName: '',
            points: ''
        };
    }

    handleSubmit () {

    }

    render () {
        let loginClasses = {
            stateRoot: 'zl-state--login login-state',
            masterKeeper: 'login-state__master-keeper login-master',
            hero: 'login-master__hero',
            form: 'login-master__form login-form',
            field: 'login-form__filed',
            btn: 'login-form__submit'
        };

        return (
            <div className={loginClasses.stateRoot}>
               <div className={loginClasses.masterKeeper}>
                   <div className={loginClasses.hero}>inzly</div>
                   <div className={loginClasses.form}>
                       <div className={loginClasses.field}>
                           <input type="text" placeholder="Имя" value={this.state.userName}/>
                       </div>
                       <div className={loginClasses.field}>
                           <input type="text" placeholder="Кол-во очков" value={this.state.points}/>
                       </div>
                       <div className={loginClasses.btn} onClick={this.handleSubmit}>Войти</div>
                   </div>
               </div>
            </div>
        );
    }
}

